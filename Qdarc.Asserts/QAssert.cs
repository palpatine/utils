﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Qdarc.Asserts
{
    [DebuggerStepThrough]
    public static class QAssert
    {
        public static void AllCorrespondingElementsShouldBeEqual<T>(
            this IEnumerable<T> collection,
            IEnumerable<T> other)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            if (!collection.SequenceEqual(other))
            {
                throw new AssertException("Collections should be sequence equal.");
            }
        }

        public static void AllCorrespondingElementsShouldBeSameInstance<T>(
            this IEnumerable<T> collection,
            IEnumerable<T> other)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            if (collection.Count() != other.Count())
            {
                throw new AssertException("Collections should of the same length.");
            }

            for (var i = 0; i < collection.Count(); i++)
            {
                if (!ReferenceEquals(collection.ElementAt(i), other.ElementAt(i)))
                {
                    throw new AssertException($"Elements at position {i} are not the same instance.");
                }
            }
        }

        public static T ShouldBeSingle<T>(this IEnumerable<T> collection, string additionalMessage = "")
        {
            var coll = collection.ToArray();
            if (coll.Count() != 1)
            {
                throw new AssertException($"Collection does not contain exactly one element. actual count {coll.Count()}. {additionalMessage}");
            }

            return coll.Single();
        }

        public static void ShouldBeEmpty<T>(this IEnumerable<T> collection, string additionalMessage = "")
        {
            if (collection.Any())
            {
                throw new AssertException($"Collection is not empty. actual count {collection.Count()}. {additionalMessage}");
            }
        }

        public static void ShouldBeEqual<T>(this T actual, T expected, string additionalMessage = "")
        {
            if (expected == null && actual == null)
            {
                return;
            }

            if (typeof(T) == typeof(Type))
            {
                CompareTypes(actual, expected, additionalMessage);
                return;
            }

            if (CompareByEquals(actual, expected, additionalMessage))
            {
                return;
            }

            var type = ((object)actual ?? expected).GetType();
            if (type.GetTypeInfo().IsClass && type != typeof(string))
            {
                CompareObjects(actual, expected, additionalMessage);
            }
            else if ((actual != null && !actual.Equals(expected)) || !expected.Equals(actual))
            {
                var message =
                    $"Should be equal assert failed. Expected: {expected}, actual {actual}. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldBeFalse(this bool actual, string additionalMessage = "")
        {
            ShouldBeEqual(actual, false, additionalMessage);
        }

        public static TExpected ShouldBeInstanceOf<TExpected>(this object actual, string additionalMessage = "")
        {
            if (actual == null)
            {
                var message = $"Should be instance of {typeof(TExpected).FullName} but is NULL. {additionalMessage}";
                throw new AssertException(message);
            }

            if (!typeof(TExpected).GetTypeInfo().IsAssignableFrom(actual.GetType().GetTypeInfo()))
            {
                var message = $"Should be instance of {typeof(TExpected).FullName} but is {actual.GetType().FullName}. {additionalMessage}";
                throw new AssertException(message);
            }

            return (TExpected)actual;
        }

        public static void ShouldBeNull<T>(this T value, string additionalMessage = "")
        {
            if (value != null)
            {
                var message = $"Should be null assert failed. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldBeTheSameInstance<T>(this T first, T second, string additionalMessage = "")
            where T : class
        {
            if (!ReferenceEquals(first, second))
            {
                var message = $"Should be the same instance assert failed. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldNotBeTheSameInstance<T>(this T first, T second, string additionalMessage = "")
            where T : class
        {
            if (ReferenceEquals(first, second))
            {
                var message = $"Should not be the same instance assert failed. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldBeTrue(this bool actual, string additionalMessage = "")
        {
            ShouldBeEqual(actual, true, additionalMessage);
        }

        public static void ShouldNotBeInstanceOf<TNotExpected>(this object actual, string additionalMessage = "")
        {
            if (typeof(TNotExpected).GetTypeInfo().IsAssignableFrom(actual.GetType().GetTypeInfo()))
            {
                var message =
                    $"Should not be instance of {typeof(TNotExpected).FullName} assert failed. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldNotBeNull<T>(this T? value, string additionalMessage = "")
                    where T : struct
        {
            if (value == null)
            {
                var message = $"Should not be null assert failed. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldNotBeNull<T>(this T value, string additionalMessage = "")
            where T : class
        {
            if (value == null)
            {
                var message = $"Should not be null assert failed. {additionalMessage}";
                throw new AssertException(message);
            }
        }

        public static void ShouldThrow<T>(Func<object> function, Func<T, bool> isCorrect = null)
            where T : Exception
        {
            try
            {
                function();
            }
            catch (T exception)
            {
                if (isCorrect?.Invoke(exception) ?? true)
                {
                    return;
                }

                throw new AssertException("Exception was thrown but does not meet acceptance criteria.", exception);
            }
            catch (Exception ex)
            {
                throw new AssertException("Exception was not thrown by given function.", ex);
            }

            throw new AssertException("Exception was not thrown by given function.");
        }

        public static void ShouldThrow<T>(Action action, Func<T, bool> isCorrect = null)
        where T : Exception
        {
            try
            {
                action();
            }
            catch (T exception)
            {
                if (isCorrect?.Invoke(exception) ?? true)
                {
                    return;
                }

                throw new AssertException("Exception was thrown but does not meet acceptance criteria.", exception);
            }
            catch (Exception ex)
            {
                throw new AssertException("Exception was not thrown by given action.", ex);
            }

            throw new AssertException("Exception was not thrown by given action.");
        }

        public static async Task ShouldThrowAsync<T>(Func<Task> function, Func<T, bool> isCorrect = null)
                            where T : Exception
        {
            try
            {
                await function();
            }
            catch (T exception)
            {
                if (isCorrect?.Invoke(exception) ?? true)
                {
                    return;
                }

                throw new AssertException("Exception was thrown but does not meet acceptance criteria.", exception);
            }
            catch (Exception ex)
            {
                throw new AssertException("Exception was not thrown by given function.", ex);
            }

            throw new AssertException("Exception was not thrown by given function.");
        }

        public static async Task ShouldThrowAsync<TException, TResult>(Func<Task<TResult>> function, Func<TException, bool> isCorrect = null)
            where TException : Exception
        {
            try
            {
                await function();
            }
            catch (TException exception)
            {
                if (isCorrect?.Invoke(exception) ?? true)
                {
                    return;
                }

                throw new AssertException("Exception was thrown but does not meet acceptance criteria.", exception);
            }
            catch (Exception ex)
            {
                throw new AssertException("Exception was not thrown by given function.", ex);
            }

            throw new AssertException("Exception was not thrown by given function.");
        }

        private static bool CompareByEquals<T>(T actual, T expected, string additionalMessage)
        {
            var equalsMethod = typeof(T).GetTypeInfo().GetDeclaredMethods("Equals")
                .SingleOrDefault(
                    x => x.GetParameters().Count() == 1 && x.GetParameters()[0].ParameterType == typeof(object));
            if (equalsMethod != null)
            {
                if ((actual != null && actual.Equals(expected)) || (expected != null && expected.Equals(actual)))
                {
                    return true;
                }

                var message =
                    $"Should be equal assert failed. Objects are not equal according to equals method. actual: {actual}, expected: {expected}. {additionalMessage}";
                throw new AssertException(message);
            }

            return false;
        }

        private static void CompareObjects<T>(T actual, T expected, string additionalMessage)
        {
            if (actual == null || expected == null)
            {
                var message =
                    $"Should be equal assert failed. One of the objects is null. actual: {actual?.ToString() ?? "null"}, expected: {expected?.ToString() ?? "null"}. {additionalMessage}";
                throw new AssertException(message);
            }

            foreach (var property in typeof(T).GetRuntimeProperties())
            {
                var message = $"{property.Name}. {additionalMessage}";
                property.GetValue(actual).ShouldBeEqual(property.GetValue(expected), message);
            }
        }

        private static void CompareTypes<T>(T actual, T expected, string additionalMessage)
        {
            var actualType = (Type)(object)actual;
            var expectedType = (Type)(object)expected;

            if (actualType == expectedType)
            {
                return;
            }

            var message =
                $"Should be equal assert failed. Objects are not equal according to equals method. actual: {actualType?.FullName ?? "null"}, expected: {expectedType?.FullName ?? "null"}. {additionalMessage}";
            throw new AssertException(message);
        }
    }
}