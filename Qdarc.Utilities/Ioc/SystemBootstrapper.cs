﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Qdarc.Utilities.Ioc
{
    /// <summary>
    /// Botstraps system using given registrar and set of assemblies that are to be scanned for
    /// bootstrappers.
    /// </summary>
    public class SystemBootstrapper
    {
        /// <summary>
        /// The registrar that will be used to register all system parts.
        /// </summary>
        private readonly IRegistrar _registrar;

        /// <summary>
        /// The system assemblies that may contain bootstrappers.
        /// </summary>
        private readonly IEnumerable<Assembly> _systemAssemblies;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemBootstrapper"/> class.
        /// </summary>
        /// <param name="registrar">The registrar.</param>
        /// <param name="systemAssemblies">The system assemblies.</param>
        public SystemBootstrapper(
            IRegistrar registrar,
            IEnumerable<Assembly> systemAssemblies)
        {
            _registrar = registrar;
            _systemAssemblies = systemAssemblies;
        }

        /// <summary>
        /// Bootstraps the system.
        /// </summary>
        public void Bootstrap()
        {
            var bootstrappers = _systemAssemblies
                .SelectMany(x => x.ExportedTypes)
                .Where(x =>
                {
                    var typeInfo = x.GetTypeInfo();
                    return typeInfo.IsClass
                        && !typeInfo.IsAbstract
                        && typeof(IBootstrapper).GetTypeInfo().IsAssignableFrom(typeInfo);
                })
                .Select(Activator.CreateInstance)
                .Cast<IBootstrapper>();

            _registrar.RegisterInstance(_registrar);

            foreach (var item in bootstrappers.OrderBy(x => x.Order))
            {
                item.Bootstrap(_registrar);
            }
        }
    }
}