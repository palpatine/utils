﻿namespace Qdarc.Utilities.Ioc
{
    /// <summary>
    /// Abstracts all classes that are a part of system initalization process.
    /// </summary>
    public interface IBootstrapper
    {
        /// <summary>
        /// Gets value that determines relative order of execution of this bootstrapper.
        /// While executing bootstrappers are arranged using this value in ascending order.
        /// </summary>
        double Order { get; }

        /// <summary>
        /// Performs initalization of part of the system.
        /// </summary>
        /// <param name="registrar">Registrar that enables IOC configuration.</param>
        void Bootstrap(IRegistrar registrar);
    }
}