﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qdarc.Utilities
{
    /// <summary>
    /// Factory class for generic equality compareres.
    /// </summary>
    public static class GenericEqualityComparer
    {
        /// <summary>
        /// Creates the specified compare function.
        /// </summary>
        /// <typeparam name="T">Type of compared entity.</typeparam>
        /// <param name="compareFunction">The compare function.</param>
        /// <param name="hashFunction">The hash function.</param>
        /// <returns>Equality comparer.</returns>
        public static IEqualityComparer<T> Create<T>(
            Func<T, T, bool> compareFunction,
            Func<T, int> hashFunction)
        {
            return new GenericEqualityComparer<T>(compareFunction, hashFunction);
        }

        /// <summary>
        /// Creates the specified compare function.
        /// </summary>
        /// <typeparam name="T">Type of compared entity.</typeparam>
        /// <param name="compareFunction">The compare function.</param>
        /// <returns>Equality comparer.</returns>
        public static IEqualityComparer<T> Create<T>(Func<T, T, bool> compareFunction)
        {
            return new GenericEqualityComparer<T>(compareFunction);
        }

        /// <summary>
        /// Retrives distinct set of elements determined by given identity retriver.
        /// </summary>
        /// <typeparam name="TElement">Type of collection.</typeparam>
        /// <typeparam name="TCompared">Type to compare.</typeparam>
        /// <param name="set">The set.</param>
        /// <param name="identityRetriever">Function that returns value that identyfies element uniqunes.</param>
        /// <returns>
        /// Distinct set of elements.
        /// </returns>
        public static IEnumerable<TElement> Distinct<TElement, TCompared>(
            this IEnumerable<TElement> set,
            Func<TElement, TCompared> identityRetriever)
        {
            return set.Distinct(Create<TElement>((x, y) => object.Equals(identityRetriever(x), identityRetriever(y)), x => identityRetriever(x).GetHashCode()));
        }

        /// <summary>
        /// Retrives all elements from first set that do not have corresponding element in second set.
        /// </summary>
        /// <typeparam name="TElement">Type of elements in collections.</typeparam>
        /// <typeparam name="TCompared">Type to compare.</typeparam>
        /// <param name="set">The first set.</param>
        /// <param name="second">The second set.</param>
        /// <param name="identityRetriever">Function that returns value that identyfies element uniqunes.</param>
        /// <returns>
        /// Distinct set of elements.
        /// </returns>
        public static IEnumerable<TElement> Except<TElement, TCompared>(
            this IEnumerable<TElement> set,
            IEnumerable<TElement> second,
            Func<TElement, TCompared> identityRetriever)
        {
            return set.Except(second, Create<TElement>((x, y) => object.Equals(identityRetriever(x), identityRetriever(y)), x => identityRetriever(x).GetHashCode()));
        }

        /// <summary>
        /// Checks if collection contains given element using given identity retriver.
        /// </summary>
        /// <typeparam name="TElement">Type of elemetns in collection.</typeparam>
        /// <typeparam name="TCompared">Type to compare.</typeparam>
        /// <param name="set">The collection.</param>
        /// <param name="element">The element to find.</param>
        /// <param name="identityRetriever">Function that returns value that identyfies element uniqunes.</param>
        /// <returns>True if corresponding item to element is found in collection; otherwise false. </returns>
        public static bool Contains<TElement, TCompared>(
            this IEnumerable<TElement> set,
            TElement element,
            Func<TElement, TCompared> identityRetriever)
        {
            return set.Contains(element, Create<TElement>((x, y) => object.Equals(identityRetriever(x), identityRetriever(y)), x => identityRetriever(x).GetHashCode()));
        }
    }
}