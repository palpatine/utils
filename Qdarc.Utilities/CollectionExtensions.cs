﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qdarc.Utilities
{
    /// <summary>
    /// Set of extensions for generic collections.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Creates cartesian prduct of two sets.
        /// </summary>
        /// <typeparam name="TFirst">First set type.</typeparam>
        /// <typeparam name="TSecond">Second set type.</typeparam>
        /// <param name="first">First set.</param>
        /// <param name="second">Second set.</param>
        /// <returns>Cartesian product of given sets.</returns>
        public static IEnumerable<Tuple<TFirst, TSecond>> CrossJoin<TFirst, TSecond>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second)
        {
            return first.CrossJoin(second, Tuple.Create);
        }

        /// <summary>
        /// Creates cartesian prduct of two sets allowing custom product selection.
        /// </summary>
        /// <typeparam name="TFirst">First set type.</typeparam>
        /// <typeparam name="TSecond">Second set type.</typeparam>
        /// <typeparam name="TResult">Type of product.</typeparam>
        /// <param name="first">First set.</param>
        /// <param name="second">Second set.</param>
        /// <param name="creator">Function that produces product based on given elements from both sets.</param>
        /// <returns>Cartesian product of given sets.</returns>
        public static IEnumerable<TResult> CrossJoin<TFirst, TSecond, TResult>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second,
            Func<TFirst, TSecond, TResult> creator)
        {
            return first.SelectMany(x => second.Select(z => creator(x, z)));
        }

        /// <summary>
        /// Returns empty collection if parameter is null.
        /// </summary>
        /// <typeparam name="TElement">Type of element.</typeparam>
        /// <param name="collection">Collection.</param>
        /// <returns>Collection of empty collection if paremeter was null.</returns>
        public static IEnumerable<TElement> EmptyIfNull<TElement>(this IEnumerable collection)
        {
            return collection != null ? collection.Cast<TElement>() : Enumerable.Empty<TElement>();
        }

        /// <summary>
        /// Returns empty collection if parameter is null.
        /// </summary>
        /// <typeparam name="TElement">Type of element.</typeparam>
        /// <param name="collection">Collection.</param>
        /// <returns>Collection of empty collection if paremeter was null.</returns>
        public static IEnumerable<TElement> EmptyIfNull<TElement>(this IEnumerable<TElement> collection)
        {
            return collection ?? Enumerable.Empty<TElement>();
        }

        /// <summary>
        /// Performs full join on two sets using keys retrived from each element.
        /// Result is determined function accepting matching elements and key.
        /// </summary>
        /// <typeparam name="TFirst">Type of elements in first set.</typeparam>
        /// <typeparam name="TSecond">Type of elements in second set.</typeparam>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <typeparam name="TResult">Type of result.</typeparam>
        /// <param name="first">First set.</param>
        /// <param name="second">Second set.</param>
        /// <param name="firstKey">Function returning key from element in first set.</param>
        /// <param name="secondKey">Function returning key from element in second set.</param>
        /// <param name="result">Function creating result based on matching elements and key.</param>
        /// <returns>Collection of objects created based on matching elemnts and keys.</returns>
        public static IEnumerable<TResult> FullJoin<TFirst, TSecond, TKey, TResult>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second,
            Func<TFirst, TKey> firstKey,
            Func<TSecond, TKey> secondKey,
            Func<TFirst, TSecond, TKey, TResult> result)
        {
            var firstLookup = first.ToLookup(firstKey);
            var secondLookup = second.ToLookup(secondKey);
            var keys = new HashSet<TKey>(firstLookup.Select(p => p.Key));
            keys.UnionWith(secondLookup.Select(p => p.Key));

            return (from key in keys
                    from firstElement in firstLookup[key].DefaultIfEmpty()
                    from secondElement in secondLookup[key].DefaultIfEmpty()
                    select result(firstElement, secondElement, key)).ToList();
        }

        /// <summary>
        /// Performs full join on two sets using keys retrived from each element.
        /// Result is determined function accepting matching elements.
        /// </summary>
        /// <typeparam name="TFirst">Type of elements in first set.</typeparam>
        /// <typeparam name="TSecond">Type of elements in second set.</typeparam>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <typeparam name="TResult">Type of result.</typeparam>
        /// <param name="first">First set.</param>
        /// <param name="second">Second set.</param>
        /// <param name="firstKey">Function returning key from element in first set.</param>
        /// <param name="secondKey">Function returning key from element in second set.</param>
        /// <param name="result">Function creating result based on matching elements.</param>
        /// <returns>Collection of objects created based on matching elemnts.</returns>
        public static IEnumerable<TResult> FullJoin<TFirst, TSecond, TKey, TResult>(
            this IEnumerable<TFirst> first,
            IEnumerable<TSecond> second,
            Func<TFirst, TKey> firstKey,
            Func<TSecond, TKey> secondKey,
            Func<TFirst, TSecond, TResult> result)
        {
            var firstLookup = first.ToLookup(firstKey);
            var secondLookup = second.ToLookup(secondKey);
            var keys = new HashSet<TKey>(firstLookup.Select(p => p.Key));
            keys.UnionWith(secondLookup.Select(p => p.Key));

            return (from key in keys
                    from firstElement in firstLookup[key].DefaultIfEmpty()
                    from secondElement in secondLookup[key].DefaultIfEmpty()
                    select result(firstElement, secondElement)).ToList();
        }

        /// <summary>
        /// Performs asynchronous projection on every element in collection.
        ///
        /// Method iterates over all items in collection.
        /// </summary>
        /// <typeparam name="TElement">Type of element in collection.</typeparam>
        /// <typeparam name="TProjection">Type of element in result.</typeparam>
        /// <param name="collection">Collection to iterate.</param>
        /// <param name="retriever">Function returning task to perfom on collection item.</param>
        /// <returns>Colection of results.</returns>
        public static async Task<IList<TProjection>> SelectAsync<TElement, TProjection>(
            this IEnumerable<TElement> collection,
            Func<TElement, Task<TProjection>> retriever)
        {
            var result = new List<TProjection>();

            foreach (var item in collection)
            {
                var inner = await retriever(item);
                result.Add(inner);
            }

            return result;
        }

        /// <summary>
        /// Performs asynchronous multiple projection on every element in collection.
        ///
        /// Method iterates over all items in collection.
        /// </summary>
        /// <typeparam name="TElement">Type of element in collection.</typeparam>
        /// <typeparam name="TProjection">Type of element in result.</typeparam>
        /// <param name="collection">Collection to iterate.</param>
        /// <param name="retriever">Function returning task to perfom on collection item.</param>
        /// <returns>Colection of results.</returns>
        public static async Task<IList<TProjection>> SelectManyAsync<TElement, TProjection>(
                this IEnumerable<TElement> collection,
                Func<TElement, Task<IEnumerable<TProjection>>> retriever)
        {
            var result = new List<TProjection>();

            foreach (var item in collection)
            {
                var inner = await retriever(item);
                result.AddRange(inner);
            }

            return result;
        }

        /// <summary>
        /// Splits two collectons based on predicate.
        /// </summary>
        /// <typeparam name="TElement">Type of element in colection.</typeparam>
        /// <param name="collection">colection to enumerate.</param>
        /// <param name="discriminator">Dyscriminator.</param>
        /// <returns>Tuple of collections where first one contains elements that are true according to dyscriminator function.</returns>
        public static Tuple<IList<TElement>, IList<TElement>> Split<TElement>(
            this IEnumerable<TElement> collection,
            Func<TElement, bool> discriminator)
        {
            var trueElements = new List<TElement>();
            var falseElements = new List<TElement>();

            foreach (var item in collection)
            {
                if (discriminator(item))
                {
                    trueElements.Add(item);
                }
                else
                {
                    falseElements.Add(item);
                }
            }

            return new Tuple<IList<TElement>, IList<TElement>>(trueElements, falseElements);
        }
    }
}