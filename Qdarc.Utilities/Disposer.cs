﻿using System;
using System.Diagnostics;

namespace Qdarc.Utilities
{
    /// <summary>
    /// Generic disposer that perfoms an action on dispose.
    /// </summary>
    public sealed class Disposer : IDisposable
    {
        private readonly Action _dispose;

        /// <summary>
        /// Creates an instance of Disposer.
        /// </summary>
        /// <param name="dispose">Action to execute on dispose.</param>
        public Disposer(Action dispose) => _dispose = dispose ?? throw new ArgumentNullException(nameof(dispose));

        /// <summary>
        /// Perfoms predefined dispose action.
        /// </summary>
        public void Dispose() => _dispose();
    }
}