﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qdarc.Utilities
{
    /// <summary>
    /// Aggregates multiple disposable object into single disposable object.
    /// </summary>
    public sealed class GroupDisposer : IDisposable
    {
        private readonly IEnumerable<IDisposable> _elements;

        /// <summary>
        /// Creates instance of <see cref="GroupDisposer"/>.
        /// </summary>
        /// <param name="elements">Disposable objects.</param>
        public GroupDisposer(IEnumerable<IDisposable> elements)
        {
            if (elements == null)
            {
                throw new ArgumentNullException(nameof(elements));
            }

            _elements = elements.ToArray();
        }

        /// <summary>
        /// Creates instance of <see cref="GroupDisposer"/>.
        /// </summary>
        /// <param name="elements">Disposable objects.</param>
        public GroupDisposer(params IDisposable[] elements)
        {
            _elements = elements;
        }

        /// <summary>
        /// Gets a value indicating whether object was already disposed.
        /// </summary>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Disposes all disposable elements.
        /// Call to disposable objects is done only once regardles of count of calls to this method.
        /// </summary>
        public void Dispose()
        {
            if (IsDisposed)
            {
                return;
            }

            foreach (var item in _elements)
            {
                item.Dispose();
            }

            IsDisposed = true;
        }
    }
}