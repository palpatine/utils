﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Asserts.Tests
{
    [TestClass]
    public class AssertsTests
    {
        [TestMethod]
        [ExpectedException(typeof(AssertException))]
        public void CorrespondingElementsComparisonShouldFailOnNotEqualSequences()
        {
            var collection1 = new[] { "a", "b", "c", "d" };
            var collection2 = new[] { "d", "a", "b", "c" };

            collection1.AllCorrespondingElementsShouldBeEqual(collection2);
        }

        [TestMethod]
        [ExpectedException(typeof(AssertException))]
        public void CorrespondingElementsComparisonShouldFailWhenSequencesHaveDifferentElementCount()
        {
            var collection1 = new[] { "a", "b", "c", "d" };
            var collection2 = new[] { "a", "b", "c" };

            collection1.AllCorrespondingElementsShouldBeEqual(collection2);
        }

        [TestMethod]
        public void CorrespondingElementsComparisonShouldSucceedOnEqualSequences()
        {
            var collection1 = new[] { "a", "b", "c", "d" };
            var collection2 = new[] { "a", "b", "c", "d" };

            collection1.AllCorrespondingElementsShouldBeEqual(collection2);
        }

        [TestMethod]
        [ExpectedException(typeof(AssertException))]
        public void FalseAssertShouldFailOnTrueValue()
        {
            var value = true;
            value.ShouldBeFalse();
        }

        [TestMethod]
        public void FalseAssertShouldSucceedOnFalseValue()
        {
            var value = false;
            value.ShouldBeFalse();
        }

        [TestMethod]
        public void ReferenceTypesCanBeCompared()
        {
            typeof(AssertsTests).ShouldBeEqual(typeof(AssertsTests));
        }

        [TestMethod]
        public void StructuralTypesCanBeCompared()
        {
            typeof(int).ShouldBeEqual(typeof(int));
        }

        [TestMethod]
        [ExpectedException(typeof(AssertException))]
        public void TrueAssertShouldFailOnFalseValue()
        {
            var value = false;
            value.ShouldBeTrue();
        }

        [TestMethod]
        public void TrueAssertShouldSucceedOnTrueValue()
        {
            var value = true;
            value.ShouldBeTrue();
        }
    }
}