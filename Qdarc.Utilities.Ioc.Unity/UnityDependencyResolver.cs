﻿using System;
using System.Collections.Generic;
using Unity;

namespace Qdarc.Utilities.Ioc.Unity
{
    /// <summary>
    /// Class of dependency resolver unity.
    /// </summary>
    public sealed class UnityDependencyResolver : IDependencyResolver
    {
        /// <summary>
        /// Private instance implementing IUnityContainer interface.
        /// </summary>
        private readonly IUnityContainer _container;

        /// <summary>
        /// Initializes a new instance of the UnityDependencyResolver class.
        /// </summary>
        /// <param name="container">Existing container with objects.</param>
        public UnityDependencyResolver(IUnityContainer container)
        {
            this._container = container;
        }

        /// <summary>
        /// Inject object of T type.
        /// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
        /// <param name="existing">Instance of existing object.</param>
        public void Inject<T>(T existing)
        {
            _container.BuildUp(existing);
        }

        /// <summary>
        /// Register new instance of T type.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <param name="instance">Instance of T type.</param>
        public void Register<T>(T instance)
        {
            _container.RegisterInstance(instance);
        }

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <param name="type">Type of dependence.</param>
        /// <returns>Resolved object.</returns>
        public T Resolve<T>(Type type)
        {
            return (T)_container.Resolve(type);
        }

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <param name="type">Type of dependence.</param>
        /// <param name="name">Type name.</param>
        /// <returns>
        /// Resolved object.
        /// </returns>
        public object Resolve(
            Type type,
            string name)
        {
            return _container.Resolve(type, name);
        }

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <returns>Resolved object.</returns>
        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// Resolved object.
        /// </returns>
        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }

        /// <summary>
        /// Resolve a dependence.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <param name="name">Type name.</param>
        /// <returns>Resolved object.</returns>
        public T Resolve<T>(string name)
        {
            return _container.Resolve<T>(name);
        }

        /// <summary>
        /// Resolve a dependences.
        /// </summary>
        /// <typeparam name="T">T type.</typeparam>
        /// <returns>Collection of resolved objects.</returns>
        public IEnumerable<T> ResolveAll<T>()
        {
            var namedInstances = _container.ResolveAll<T>();

            return namedInstances;
        }
    }
}